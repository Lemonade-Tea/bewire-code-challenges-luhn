<?php
// phpcs:ignoreFile

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use src\Luhn;

class LuhnTest extends TestCase
{
    private function _luhnBuilder()
    {
        return new Luhn();
    }

    private function testEvaluate($number, $expected = false)
    {
        $luhn = $this->_luhnBuilder();
        $this->assertEquals($expected, $luhn->evaluate($number));
    }

    /**
     * @dataProvider provideExamples
     */
    public function testEvaluateShouldReturnCorrectValue($number, $expected)
    {
        $this->testEvaluate($number, $expected);
    }

    /**
     * @dataProvider provideExamplesContainingLetters
     */
    public function testEvaluateShouldReturnFalseForStringsWithLetters($number)
    {
        $this->testEvaluate($number);
    }

    /**
     * @dataProvider provideExamplesContaininglessThanTwoChars
     */
    public function testEvaluateShouldReturnFalseForNumbersWithLessThanTwoChars($number)
    {
        $this->testEvaluate($number);
    }

    /**
     * @dataProvider provideMultiplesOf10
     */
    public function testShouldReturnTrueForMultiplesOf10(int $number)
    {
        $luhn = $this->_luhnBuilder();

        $result = $luhn->isDividableBy10($number);
        $this->assertEquals(true, $result);
    }

    /**
     * @dataProvider provideDoublerExamples
     */
    public function testShouldReturnDoubleOfValue(Int $value, Int $expected)
    {
        $luhn = $this->_luhnBuilder();
        $result = $luhn->doubler($value);

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider provideValidateDoubleExamples
     */
    public function testValidateDoubleShouldReturnCorrectValue($value, $expected)
    {
        $luhn = $this->_luhnBuilder();

        $result = $luhn->validateDouble($value);

        $this->assertEquals($expected, $result);
    }


    /**
     * @dataProvider provideSanitizeStringExamples
     */
    public function testSanitizeString($value, $expected)
    {
        $luhn = $this->_luhnBuilder();

        $result = $luhn->sanitizeString($value);

        $this->assertEquals($expected, $result);
    }

    public function provideMultiplesOf10()
    {
        return [[10], [20], [30]];
    }

    public function provideExamples()
    {
        return [
            ['1', false],
            ['055 444 285', true],
            ['055 444 286', false],
            ['091', true],
            ['095 245 88', true],
            ['234 567 891 234', true]
        ];
    }

    public function provideExamplesContainingLetters()
    {
        return [
            ['53a1Id'],
            ['45669e'],
            ['IkBenEenEchtGetalHoor'],
            ['true']
        ];
    }

    public function provideExamplesContaininglessThanTwoChars()
    {
        return [['0'], ['1'], ['8'], ['\'']];
    }

    public function provideDoublerExamples()
    {
        return [[2, 4], [5, 1], [9, 9], [7, 5]];
    }

    public function provideValidateDoubleExamples()
    {
        return [[4, 4], [10, 1], [18, 9]];
    }

    public function provideSanitizeStringExamples()
    {
        return [
            ['412 12 2', '412122'],
            ['2      1', '21'],
            ['5432 2346 9786 0623', '5432234697860623']
        ];
    }
}
