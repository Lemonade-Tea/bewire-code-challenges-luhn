<?php

/**
 * Luhn
 * php version 7.3.27
 *
 * @category Class
 * @author   Gwen Nijssen <gwen.nijssen@evance.be>
 */

namespace src;

/**
 * Luhn Algorithm
 *
 * @category Class
 * @author   Gwen Nijssen <gwen.nijssen@evance.be>
 */
class Luhn
{
    /**
     * Main function to be executed.
     * Will check if the given string is a valid
     * identification number following the Luhn algorithm.
     *
     * Returns a boolean value of `true` or `false` depending if the number is valid.
     *
     * @param String $number Input number
     *
     * @return Boolean true or false
     */
    public function evaluate(String $number): bool
    {
        $sanitizedString = $this->sanitizeString($number);

        if ($sanitizedString > 1 && ctype_digit($sanitizedString)) {
            $sum = $this->_luhnSum($sanitizedString);
            return $this->isDividableBy10($sum);
        }

        return false;
    }

    /**
     * Sums the given string of numbers following the Luhn algorithm
     *
     * @param String $number Input number.
     *
     * @return Int Sum of numbers
     */
    private function _luhnSum(String $number): Int
    {
        $sum = 0;
        $isOdd = true;

        for ($iterator = strlen($number); $iterator > 0; $iterator--) {

            if ($isOdd) {
                $sum += $number[$iterator - 1];
            } else {
                $sum += $this->doubler($number[$iterator - 1]);
            }

            $isOdd = !$isOdd;
        }

        return $sum;
    }

    /**
     * Checks if the value given is dividable by 10
     *
     * Returns a boolean value whether the value given is dividable by 10 or not
     *
     * @param Int $number Integer to be checked.
     *
     * @return Boolean true or false
     */
    public function isDividableBy10(Int $number): bool
    {
        return $number % 10 === 0;
    }

    /**
     * Doubles the given number and returns a single digit value.
     * If the doubled value is higher than 9,
     * it will subtract 9 from that and return the result.
     *
     * @param Int $number Number to be doubled.
     *
     * @return Int Single digit number between 1 to 9.
     */
    public function doubler(Int $number): Int
    {
        return $this->validateDouble($number * 2);
    }

    /**
     * Validates the given number.
     * If the number is more than 9 it will return the number subtracted by 9.
     * If it is less than 9 it will return the given number.
     *
     * For example:
     * `Luhn->validateDouble(12);` will return 3
     *
     * @param Int $number Number to be validated
     *
     * @return Int
     */
    public function validateDouble(Int $number): Int
    {
        return $number > 9 ? $number - 9 : $number;
    }

    /**
     * Sanitizes the string from any spaces and returns sanitized string.
     *
     * @param String $string Input string.
     *
     * @return String Sanitized string without spaces.
     */
    public function sanitizeString(String $string): String
    {
        return str_replace(' ', '', $string);
    }
}
