---
source: src/Luhn.php
won: true
---

# Bewire Code-Challenge: Luhn

> PHP version - [7.3.27](https://www.php.net/downloads#v7.3.27)

> Composer version - [2.0.11](https://getcomposer.org/download/)

## Info

Voor alle informatie van deze challenge kan je terecht bij de [Code Challenges](https://code-challenges.bewire.org/challenges/luhn/) webiste van Bewire.

- Er is ook een [versimpelde versie](luhn.md) in deze repository.

## Setup

Run composer in root directory `./luhn/GwenNijssen_PHP/` to install phpunit.

> ```Bash
> composer install
> ```

Run phpunit

> ```Bash
> ./vendor/bin/phpunit ./tests/LuhnTest.php
> ```

## Denkwijze

Als eerst sanitize ik de input van spaties. Daarna check ik of de input string groter is dan 1 EN enkel characters bevat die decimals zijn.

- Indien de input kleiner is dan 2 of deze letters bevat, stopt de functie en return ik meteen false.

Vervolgens iterate ik de string van rechts naar links en som ik deze op.

- Dit doe ik d.m.v. de iterator gelijk te stellen aan de lengte van de sanitized string en vervolgens via een decrement operator doorheen de forloop te gaan.

Bij elke tweede iteratie verdubbel ik eerst het getal en check ik of het groter dan 9 is.

Als de verdubbeling groter is dan 9, verminder ik deze met 9 en geef ik uiteindelijk de uitkomst terug en tel deze op bij de som.

Als laatste stap check ik of de som deelbaar is door 10 en geef een boolean terug.
