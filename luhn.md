---
title: Luhn
starts: 2021-03-17
ends: 2021-03-31
voting_ends: 2021-04-14
---

Gegeven een getal, bepaal of het getal geldig is volgens de luhn formule.

Het [Luhn algoritme](https://en.wikipedia.org/wiki/Luhn_algorithm) is
een simpele checksum voor het valideren van verschillende identificatie nummers,
zoals een creditkaart nummer.

Bepaal of een getal geldig is.

**Gebruik geen libraries!**

## Valideren van een getal

Strings met een lengte 1 of kleiner zijn niet geldig. Spaties zijn toegelaten in de input,
maar deze moeten uit de input uit gehaald worden vooraleer de check gebeurt. Niet numerieke
characters zijn niet toegelaten.

## Voorbeeld 1: een geldig creditkaart nummer

```text
4539 1488 0343 6467
```

De eerste stap is het verdubbelen van elk tweede getal, beginnende van rechts.

```text
4_3_ 1_8_ 0_4_ 6_6_
```

Als de verdubbeling van een getal een uitkomst geeft groter dan 9,
verminder de uitkomst dan met 9. Het resultaat:

```text
8569 2478 0383 3437
```

De som van alle getallen:

```text
8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80
```

Als de som deelbaar is door 10, dan is het creditkaart nummer geldig.

## Voorbeeld 2: ongeldig creditkaart nummer

```text
8273 1232 7352 0569
```

Verdubbelen van elk tweede getal, startende van rechts.

```text
7253 2262 5312 0539
```

Som van de getallen

```text
7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57
```

57 is niet deelbaar door 10. Het is geen geldig nummer.

## Testen

Test je code af door unit tests te schrijven.

Voorbeelden om je oplossing te testen:

- Een getal met 1 cijfer kan niet geldig zijn. Ook als deze vooraf gegaan wordt door spaties.
- eender welk niet numeriek character maakt een nummer ongeldig
- getal met even cijfers en oneven cijfers testen

### input voorbeelden

|          nummer | geldig |
| --------------: | :----: |
|               1 |  Nee   |
|     055 444 285 |   Ja   |
|     055 444 286 |  Nee   |
|             091 |   Ja   |
|      095 245 88 |   Ja   |
| 234 567 891 234 |   Ja   |
